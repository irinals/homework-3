//весь калькулятор обёрнут в модуль
(function() {
var textArea = document.getElementById('inputVal');
//действия при нажатии кнопок с цифрами
var numButtons = document.getElementsByClassName('num');
for (let i = 0; i < numButtons.length; i++){
  numButtons[i].addEventListener('click', numButtonClick);
};
function numButtonClick() {
  if (textArea.innerHTML === '0'){
  textArea.innerHTML = this.innerHTML;
  }
  else{
  textArea.innerHTML += this.innerHTML;
  }
};
//действия при нажатии кнопок со знаками +,-,*
//с использованием bind
var add = {
  sign: '+',
  getSign: function() {
    return this.sign;
  }
};
var sub = {
  sign: '-'
};
var mult = {
  sign: '*'
};
var getSign = add.getSign;
var addGetSign = getSign.bind(add);
var subGetSign = getSign.bind(sub);
var multGetSign = getSign.bind(mult);
document.getElementById('add').addEventListener('click', addButtonClick);
document.getElementById('sub').addEventListener('click', subButtonClick);
document.getElementById('mult').addEventListener('click', multButtonClick);
function addButtonClick() {
  textArea.innerHTML += addGetSign();
};
function subButtonClick() {
  textArea.innerHTML += subGetSign();
};
function multButtonClick() {
  textArea.innerHTML += multGetSign();
};

//без использования bind
// var signButtons = document.getElementsByClassName('sign');
// for (let i = 0; i < signButtons.length; i++){
// signButtons[i].addEventListener('click', signButtonClick);
// };
// function signButtonClick() {
//   textArea.innerHTML += this.innerHTML;
// };

//действия при нажатии кнопки С
document.getElementById('dump').addEventListener('click', dumpButtonClick);
function dumpButtonClick() {
  textArea.innerHTML = '0';
};
//действия при нажатии кнопки =
var equalButton = document.getElementById('equal');
document.getElementById('equal').addEventListener('click', equalButtonClick);
function equalButtonClick() {
  textArea.innerHTML = eval(textArea.innerHTML);
};
}());
